# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = "diaspora-vines"
  s.version = "0.2.0.develop.4"

  s.required_rubygems_version = Gem::Requirement.new("> 1.3.1") if s.respond_to? :required_rubygems_version=
  s.authors = ["David Graham", "Lukas Matt"]
  s.date = "2015-10-10"
  s.description = "Diaspora-vines is a Vines fork build for diaspora integration. DO NOT use it unless you know what you are doing!"
  s.email = ["david@negativecode.com", "lukas@zauberstuhl.de"]
  s.executables = ["vines"]
  s.files = ["Gemfile", "LICENSE", "README.md", "Rakefile", "bin/vines", "conf/certs/README", "conf/certs/ca-bundle.crt", "conf/config.rb", "lib/vines.rb", "lib/vines/cli.rb", "lib/vines/cluster.rb", "lib/vines/cluster/connection.rb", "lib/vines/cluster/publisher.rb", "lib/vines/cluster/pubsub.rb", "lib/vines/cluster/sessions.rb", "lib/vines/cluster/subscriber.rb", "lib/vines/command/cert.rb", "lib/vines/command/restart.rb", "lib/vines/command/start.rb", "lib/vines/command/stop.rb", "lib/vines/config.rb", "lib/vines/config/diaspora.rb", "lib/vines/config/host.rb", "lib/vines/config/port.rb", "lib/vines/config/pubsub.rb", "lib/vines/contact.rb", "lib/vines/daemon.rb", "lib/vines/error.rb", "lib/vines/jid.rb", "lib/vines/kit.rb", "lib/vines/log.rb", "lib/vines/node.rb", "lib/vines/router.rb", "lib/vines/stanza.rb", "lib/vines/stanza/dialback.rb", "lib/vines/stanza/iq.rb", "lib/vines/stanza/iq/auth.rb", "lib/vines/stanza/iq/disco_info.rb", "lib/vines/stanza/iq/disco_items.rb", "lib/vines/stanza/iq/error.rb", "lib/vines/stanza/iq/ping.rb", "lib/vines/stanza/iq/private_storage.rb", "lib/vines/stanza/iq/query.rb", "lib/vines/stanza/iq/result.rb", "lib/vines/stanza/iq/roster.rb", "lib/vines/stanza/iq/session.rb", "lib/vines/stanza/iq/vcard.rb", "lib/vines/stanza/iq/version.rb", "lib/vines/stanza/message.rb", "lib/vines/stanza/presence.rb", "lib/vines/stanza/presence/error.rb", "lib/vines/stanza/presence/probe.rb", "lib/vines/stanza/presence/subscribe.rb", "lib/vines/stanza/presence/subscribed.rb", "lib/vines/stanza/presence/unavailable.rb", "lib/vines/stanza/presence/unsubscribe.rb", "lib/vines/stanza/presence/unsubscribed.rb", "lib/vines/stanza/pubsub.rb", "lib/vines/stanza/pubsub/create.rb", "lib/vines/stanza/pubsub/delete.rb", "lib/vines/stanza/pubsub/publish.rb", "lib/vines/stanza/pubsub/subscribe.rb", "lib/vines/stanza/pubsub/unsubscribe.rb", "lib/vines/storage.rb", "lib/vines/storage/local.rb", "lib/vines/storage/null.rb", "lib/vines/storage/sql.rb", "lib/vines/store.rb", "lib/vines/stream.rb", "lib/vines/stream/client.rb", "lib/vines/stream/client/auth.rb", "lib/vines/stream/client/auth_restart.rb", "lib/vines/stream/client/bind.rb", "lib/vines/stream/client/bind_restart.rb", "lib/vines/stream/client/closed.rb", "lib/vines/stream/client/ready.rb", "lib/vines/stream/client/session.rb", "lib/vines/stream/client/start.rb", "lib/vines/stream/client/tls.rb", "lib/vines/stream/component.rb", "lib/vines/stream/component/handshake.rb", "lib/vines/stream/component/ready.rb", "lib/vines/stream/component/start.rb", "lib/vines/stream/http.rb", "lib/vines/stream/http/auth.rb", "lib/vines/stream/http/bind.rb", "lib/vines/stream/http/bind_restart.rb", "lib/vines/stream/http/ready.rb", "lib/vines/stream/http/request.rb", "lib/vines/stream/http/session.rb", "lib/vines/stream/http/sessions.rb", "lib/vines/stream/http/start.rb", "lib/vines/stream/parser.rb", "lib/vines/stream/sasl.rb", "lib/vines/stream/server.rb", "lib/vines/stream/server/auth.rb", "lib/vines/stream/server/auth_method.rb", "lib/vines/stream/server/auth_restart.rb", "lib/vines/stream/server/final_restart.rb", "lib/vines/stream/server/outbound/auth.rb", "lib/vines/stream/server/outbound/auth_dialback_result.rb", "lib/vines/stream/server/outbound/auth_external.rb", "lib/vines/stream/server/outbound/auth_external_result.rb", "lib/vines/stream/server/outbound/auth_restart.rb", "lib/vines/stream/server/outbound/authoritative.rb", "lib/vines/stream/server/outbound/final_features.rb", "lib/vines/stream/server/outbound/final_restart.rb", "lib/vines/stream/server/outbound/start.rb", "lib/vines/stream/server/outbound/tls_result.rb", "lib/vines/stream/server/ready.rb", "lib/vines/stream/server/start.rb", "lib/vines/stream/state.rb", "lib/vines/token_bucket.rb", "lib/vines/user.rb", "lib/vines/version.rb", "lib/vines/xmpp_server.rb", "test/cluster/publisher_test.rb", "test/cluster/sessions_test.rb", "test/cluster/subscriber_test.rb", "test/config/host_test.rb", "test/config/pubsub_test.rb", "test/config_test.rb", "test/contact_test.rb", "test/error_test.rb", "test/ext/nokogiri.rb", "test/jid_test.rb", "test/kit_test.rb", "test/router_test.rb", "test/stanza/iq/disco_info_test.rb", "test/stanza/iq/disco_items_test.rb", "test/stanza/iq/private_storage_test.rb", "test/stanza/iq/roster_test.rb", "test/stanza/iq/session_test.rb", "test/stanza/iq/vcard_test.rb", "test/stanza/iq/version_test.rb", "test/stanza/iq_test.rb", "test/stanza/message_test.rb", "test/stanza/presence/probe_test.rb", "test/stanza/presence/subscribe_test.rb", "test/stanza/pubsub/create_test.rb", "test/stanza/pubsub/delete_test.rb", "test/stanza/pubsub/publish_test.rb", "test/stanza/pubsub/subscribe_test.rb", "test/stanza/pubsub/unsubscribe_test.rb", "test/stanza_test.rb", "test/storage/local_test.rb", "test/storage/mock_redis.rb", "test/storage/null_test.rb", "test/storage/sql_schema.rb", "test/storage/sql_test.rb", "test/storage/storage_tests.rb", "test/store_test.rb", "test/stream/client/auth_test.rb", "test/stream/client/ready_test.rb", "test/stream/client/session_test.rb", "test/stream/component/handshake_test.rb", "test/stream/component/ready_test.rb", "test/stream/component/start_test.rb", "test/stream/http/auth_test.rb", "test/stream/http/ready_test.rb", "test/stream/http/request_test.rb", "test/stream/http/sessions_test.rb", "test/stream/http/start_test.rb", "test/stream/parser_test.rb", "test/stream/sasl_test.rb", "test/stream/server/auth_method_test.rb", "test/stream/server/auth_test.rb", "test/stream/server/outbound/auth_dialback_result_test.rb", "test/stream/server/outbound/auth_external_test.rb", "test/stream/server/outbound/auth_restart_test.rb", "test/stream/server/outbound/auth_test.rb", "test/stream/server/outbound/authoritative_test.rb", "test/stream/server/outbound/start_test.rb", "test/stream/server/ready_test.rb", "test/stream/server/start_test.rb", "test/test_helper.rb", "test/token_bucket_test.rb", "test/user_test.rb"]
  s.homepage = "https://diasporafoundation.org"
  s.licenses = ["MIT"]
  s.require_paths = ["lib"]
  s.required_ruby_version = Gem::Requirement.new(">= 1.9.3")
  s.rubygems_version = "1.8.23"
  s.summary = "Diaspora-vines is a Vines fork build for diaspora integration."
  s.test_files = ["test/error_test.rb", "test/test_helper.rb", "test/storage/local_test.rb", "test/storage/mock_redis.rb", "test/storage/sql_schema.rb", "test/storage/sql_test.rb", "test/storage/null_test.rb", "test/storage/storage_tests.rb", "test/ext/nokogiri.rb", "test/contact_test.rb", "test/store_test.rb", "test/cluster/sessions_test.rb", "test/cluster/publisher_test.rb", "test/cluster/subscriber_test.rb", "test/config_test.rb", "test/stream/parser_test.rb", "test/stream/client/ready_test.rb", "test/stream/client/auth_test.rb", "test/stream/client/session_test.rb", "test/stream/sasl_test.rb", "test/stream/http/sessions_test.rb", "test/stream/http/start_test.rb", "test/stream/http/ready_test.rb", "test/stream/http/auth_test.rb", "test/stream/http/request_test.rb", "test/stream/component/start_test.rb", "test/stream/component/ready_test.rb", "test/stream/component/handshake_test.rb", "test/stream/server/start_test.rb", "test/stream/server/ready_test.rb", "test/stream/server/auth_test.rb", "test/stream/server/auth_method_test.rb", "test/stream/server/outbound/start_test.rb", "test/stream/server/outbound/auth_restart_test.rb", "test/stream/server/outbound/auth_external_test.rb", "test/stream/server/outbound/auth_test.rb", "test/stream/server/outbound/auth_dialback_result_test.rb", "test/stream/server/outbound/authoritative_test.rb", "test/token_bucket_test.rb", "test/router_test.rb", "test/user_test.rb", "test/stanza/message_test.rb", "test/stanza/presence/subscribe_test.rb", "test/stanza/presence/probe_test.rb", "test/stanza/pubsub/publish_test.rb", "test/stanza/pubsub/delete_test.rb", "test/stanza/pubsub/create_test.rb", "test/stanza/pubsub/unsubscribe_test.rb", "test/stanza/pubsub/subscribe_test.rb", "test/stanza/iq_test.rb", "test/stanza/iq/roster_test.rb", "test/stanza/iq/vcard_test.rb", "test/stanza/iq/disco_info_test.rb", "test/stanza/iq/private_storage_test.rb", "test/stanza/iq/session_test.rb", "test/stanza/iq/version_test.rb", "test/stanza/iq/disco_items_test.rb", "test/jid_test.rb", "test/stanza_test.rb", "test/config/host_test.rb", "test/config/pubsub_test.rb", "test/kit_test.rb"]

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<bcrypt>, ["~> 3.1"])
      s.add_runtime_dependency(%q<em-hiredis>, ["~> 0.3.0"])
      s.add_runtime_dependency(%q<eventmachine>, ["~> 1.0.8"])
      s.add_runtime_dependency(%q<http_parser.rb>, ["~> 0.6"])
      s.add_runtime_dependency(%q<nokogiri>, ["~> 1.6"])
      s.add_runtime_dependency(%q<activerecord>, ["~> 4.1"])
      s.add_development_dependency(%q<pronto>, ["~> 0.4.2"])
      s.add_development_dependency(%q<pronto-rubocop>, ["~> 0.4.4"])
      s.add_development_dependency(%q<rails>, ["~> 4.1"])
      s.add_development_dependency(%q<sqlite3>, ["~> 1.3.9"])
      s.add_development_dependency(%q<minitest>, ["~> 5.8"])
      s.add_development_dependency(%q<rake>, ["~> 10.3"])
    else
      s.add_dependency(%q<bcrypt>, ["~> 3.1"])
      s.add_dependency(%q<em-hiredis>, ["~> 0.3.0"])
      s.add_dependency(%q<eventmachine>, ["~> 1.0.8"])
      s.add_dependency(%q<http_parser.rb>, ["~> 0.6"])
      s.add_dependency(%q<nokogiri>, ["~> 1.6"])
      s.add_dependency(%q<activerecord>, ["~> 4.1"])
      s.add_dependency(%q<pronto>, ["~> 0.4.2"])
      s.add_dependency(%q<pronto-rubocop>, ["~> 0.4.4"])
      s.add_dependency(%q<rails>, ["~> 4.1"])
      s.add_dependency(%q<sqlite3>, ["~> 1.3.9"])
      s.add_dependency(%q<minitest>, ["~> 5.8"])
      s.add_dependency(%q<rake>, ["~> 10.3"])
    end
  else
    s.add_dependency(%q<bcrypt>, ["~> 3.1"])
    s.add_dependency(%q<em-hiredis>, ["~> 0.3.0"])
    s.add_dependency(%q<eventmachine>, ["~> 1.0.8"])
    s.add_dependency(%q<http_parser.rb>, ["~> 0.6"])
    s.add_dependency(%q<nokogiri>, ["~> 1.6"])
    s.add_dependency(%q<activerecord>, ["~> 4.1"])
    s.add_dependency(%q<pronto>, ["~> 0.4.2"])
    s.add_dependency(%q<pronto-rubocop>, ["~> 0.4.4"])
    s.add_dependency(%q<rails>, ["~> 4.1"])
    s.add_dependency(%q<sqlite3>, ["~> 1.3.9"])
    s.add_dependency(%q<minitest>, ["~> 5.8"])
    s.add_dependency(%q<rake>, ["~> 10.3"])
  end
end
